declare module "@salesforce/apex/Test.getDataWS" {
  export default function getDataWS(): Promise<any>;
}
declare module "@salesforce/apex/Test.getDataSF" {
  export default function getDataSF(param: {order: any}): Promise<any>;
}
