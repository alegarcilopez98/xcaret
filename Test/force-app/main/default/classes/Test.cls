public with sharing class Test {
    @AuraEnabled
    public static void getDataWS(){
        HttpRequest request = new HttpRequest();
        request.setMethod('GET');
        request.setEndPoint('https://www.reddit.com/r/subreddit/new.json?sort=new');
        
        try {
            Http http = new Http();
            HttpResponse response = new HttpResponse();
            response = http.send(request);
            
            Subreddit objSubreddit = (Subreddit) JSON.deserialize(response.getBody(), Subreddit.class);
            
            List<reddit_items__c> lstInsert = new List<reddit_items__c>();
            for(Children record:objSubreddit.data.children){
                reddit_items__c obj = new reddit_items__c();
                obj.title__c = record.data.title;
                obj.author_fullname__c = record.data.author_fullname;
                obj.thumbnail__c = record.data.thumbnail;
                obj.selftext__c = record.data.selftext;
                obj.created__c = DateTime.newInstance(Long.valueof(record.data.created));
                lstInsert.add(obj);
            }
            
            if(!lstInsert.isEmpty()){
                List<reddit_items__c> lstDelete = [SELECT Id 
                                                   FROM reddit_items__c];
                
                delete lstDelete;
                insert lstInsert;
            }
        } catch (Exception e) {
            system.debug('****error:'+e.getMessage());
        }
    }
    
    @AuraEnabled
    public static List<reddit_items__c> getDataSF(String order){
        String query = 'SELECT Id, title__c, author_fullname__c, thumbnail__c, selftext__c, created__c '+
                'FROM reddit_items__c '+
                'ORDER BY created__c '+order;
        
        return Database.query(query);
    }
    
    public Class Subreddit{
        public DataParent data;
        
    }
    
    public Class DataParent{
        public List<Children> children;
    }
    
    public Class Children{
        public DataChild data;
    }
    
    public Class DataChild{
        public String title;
        public String author_fullname;
        public String thumbnail;
        public String selftext;
        public String created;
    }
}